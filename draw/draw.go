package draw

import (
	"github.com/BigJk/ramen/console"
	"github.com/BigJk/ramen/t"
	"github.com/hajimehoshi/ebiten"
	
	"gitlab.com/Bleu-Box/freezing-chrome/colors"
	. "gitlab.com/Bleu-Box/freezing-chrome/creature"
	. "gitlab.com/Bleu-Box/freezing-chrome/cmap"
)

func DrawGame(con *console.Console, screen *ebiten.Image, player Player, cmap CMap) {
	con.ClearAll(t.Background(colors.Black))

	for pos, cell := range cmap {
		con.Print(pos.X - player.X + (WIDTH/2), pos.Y - player.Y + (HEIGHT/2),
			cell.Surf.Ch, t.Foreground(cell.Surf.Fg), t.Background(cell.Surf.Bg))
	}

	con.Print(WIDTH/2, HEIGHT/2, "@", t.Foreground(colors.Ltgray))
}
