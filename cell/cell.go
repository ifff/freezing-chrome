package cell

import (
	// . "gitlab.com/Bleu-Box/freezing-chrome/item"
	// . "gitlab.com/Bleu-Box/freezing-chrome/creature"
	. "gitlab.com/Bleu-Box/freezing-chrome/surface"
)

// This structure represents a singular space in the game map's grid. It contains all the things
// that might be on such a space, like creatures and items.
type Cell struct {
	// whether or not the player has seen this cell
	Seen bool
	Surf Surface
	// item Item 
	// crea Creature
}

// Creates a new, empty cell with the given surface.
func NewCell(surf Surface) Cell {
	return Cell {
		Seen: false,
			Surf: surf,
		}
}
