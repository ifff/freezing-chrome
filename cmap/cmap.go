package cmap

import (
	. "gitlab.com/Bleu-Box/freezing-chrome/cell"
	surf "gitlab.com/Bleu-Box/freezing-chrome/surface"
	
	"math/rand"
	"time"
)

// A Pos represents an (x, y) position on the game grid.
type Pos struct {
	X, Y int
}

const (
	WIDTH = 60
	HEIGHT = 30
)

// Type alias for a map of Cells indexed by Positions
type CMap = map[Pos]Cell

func NewMap() (int, int, CMap) {
	// seed the rng
	rand.Seed(time.Now().Unix())

	// initialize the map and generate terrain
	cmap := make(map[Pos]Cell)

	// start by filling the grid with walls
	for x := 0; x < WIDTH; x++ {
		for y := 0; y < HEIGHT; y++ {
			cmap[Pos{x, y}] = NewCell(surf.Wall)
		}
	}

	// make some squiggly paths between random points of the map
	x1 := rand.Intn(WIDTH-4) + 2
	y1 := rand.Intn(HEIGHT-4) + 2
	for i := 0; i < 15; i++ {
		x2 := rand.Intn(WIDTH-4) + 2
		y2 := rand.Intn(HEIGHT-4) + 2

		drunkenWalk(cmap, x1, y1, x2, y2)

		x1, y1 = x2, y2
	}

	// return the position the drunken walk ended up at,
	// so the player can be placed there
	return x1, y1, cmap
}

func drunkenWalk(cmap CMap, x1, y1, x2, y2 int) {
	// use a drunken walk to make a pathway that
	// starts at (x1, y1) and ends at (x2, y2)
	var dx, dy int
	if x1 < x2 { dx = +1 } else { dx = -1 }
	if y1 < y2 { dy = +1 } else { dy = -1 }

	for x, y := x1, y1; x != x2 || y != y2; {
		// "erase" the cell at (x, y)
		cmap[Pos{x, y}] = NewCell(surf.Floor)

		// randomly move either in the x or the y direction, as long as the
		// move is in-bounds and the square isn't already at its destination
		if rand.Intn(2) == 0 && x + dx < WIDTH && x + dx > 0 && x != x2 {
			x += dx
		} else if y + dy < HEIGHT && y + dy > 0 && y != y2 {
			y += dy
		}
	}
}
