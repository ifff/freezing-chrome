package main

import (
	"github.com/BigJk/ramen/console"
	"github.com/BigJk/ramen/font"
	"github.com/hajimehoshi/ebiten"

	. "gitlab.com/Bleu-Box/freezing-chrome/draw"
	. "gitlab.com/Bleu-Box/freezing-chrome/creature"
	. "gitlab.com/Bleu-Box/freezing-chrome/cmap"
)

func main() {
	// load font
	font, err := font.New("./assets/terminus-11x11.png", 11, 11)
	if err != nil {
		panic(err)
	}
	
	// create console
	con, err := console.New(50, 25, font, "freezing chrome")
	if err != nil {
		panic(err)
	}

	// set the max tick rate so the player doesn't move
	// at an uncontrollably fast speed.
	// TODO: since this is such a fiddly control, try to make it
	// customizable via command-line args later.
	// FIXME: the controls occasionally unevenly fast
	ebiten.SetMaxTPS(6)
	
	px, py, cmap := NewMap()
	player := Player{px, py}
	
	con.SetTickHook(func(timeElapsed float64) error {
		player.ProcessInput(cmap)

		return nil
	})

	// inside here is where you put all the rendering functionality
	con.SetPreRenderHook(func(screen *ebiten.Image, _ float64) error {
		DrawGame(con, screen, player, cmap)
		return nil
	})


	// start console with a scale of 1
	con.Start(1)
}
