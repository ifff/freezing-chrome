package colors

import (
	"github.com/BigJk/ramen/consolecolor"
)

type Color = consolecolor.Color

var Black = consolecolor.New(0, 0, 0)
var Ltgray = consolecolor.New(200, 200, 200)
