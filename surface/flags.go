package surface

// Surface bitmasks describe the different traits that a Surface can have.
const (
	SF_SOLID = 1 << 0 // whether the surface can be seen through / is tangible
	SF_WALKABLE = 1 << 1 // whether the surface can be walked on
)
