package surface

import (
	"gitlab.com/Bleu-Box/freezing-chrome/colors"
)

// A Surface represents the bottomost element of a cell. This usually means something like
// a floor tile, a wall, lava, liquid, etc.
type Surface struct {
	Flags int
	Descr string
	Fg colors.Color
	Bg colors.Color
	Ch string
}

/* definitions for game surfaces */
var Floor = Surface {
	Flags: SF_SOLID|SF_WALKABLE,
		Descr: "",
		Fg: colors.Ltgray,
		Bg: colors.Black,
		Ch: ".",
	}

var Wall = Surface {
	Flags: SF_SOLID,
		Descr: "",
		Fg: colors.Black,
		Bg: colors.Ltgray,
		Ch: "#",
	}
