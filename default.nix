with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "freezing-chrome";
  buildInputs = [
    libtcod
    SDL2
    pkgconfig
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXinerama
    xorg.libXi
    xorg.libXxf86vm
  ];
}
