# freezing-chrome
This repository holds code for a roguelike game. The name is inspired by William Gibson's _Burning Chrome_ book, but the game isn't cyberpunk or Gibson-inspired. I just like the name, really.

The premise of this roguelike is that you're an explorer who's found a planet covered in a thick layer of snow and ice. A small cave on this world leads down deeper into the planet's crust, where geothermal heat causes strange life forms to thrive under the inhospitable surface. However, this warm underground clime proves inhospitable to your human biology; hydrocarbon lakes, and worse, azidoazide azide, can be easily found in these depths. To worsen matters, dangerous life-forms stalk the stygian depths. Yet you must delve deeper, for an intriguing surprise awaits you at the bottom.

## running

	$ nix-shell
	$ go run main.go

