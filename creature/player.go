package creature

import (
	e "github.com/hajimehoshi/ebiten"

	. "gitlab.com/Bleu-Box/freezing-chrome/cmap"
	. "gitlab.com/Bleu-Box/freezing-chrome/surface"
)

type Player struct {
	X, Y int
}

// Process the keyboard input with regard to how it affects the player.
func (p *Player) ProcessInput(cmap CMap) {
	/* movement controls */
	if e.IsKeyPressed(e.KeyLeft) || e.IsKeyPressed(e.Key4) {
		p.move(-1, 0, cmap)
	}

	if e.IsKeyPressed(e.KeyRight) || e.IsKeyPressed(e.Key6) {
		p.move(1, 0, cmap)
	}

	if e.IsKeyPressed(e.KeyUp) || e.IsKeyPressed(e.Key8) {
		p.move(0, -1, cmap)
	}

	if e.IsKeyPressed(e.KeyDown) || e.IsKeyPressed(e.Key2) {
		p.move(0, 1, cmap)
	}

	if e.IsKeyPressed(e.Key1) {
		p.move(-1, 1, cmap)
	}

	if e.IsKeyPressed(e.Key3) {
		p.move(1, 1, cmap)
	}

	if e.IsKeyPressed(e.Key7) {
		p.move(-1, -1, cmap)
	}

	if e.IsKeyPressed(e.Key9) {
		p.move(1, -1, cmap)
	}
}

func (p *Player) move(dx, dy int, cmap CMap) {
	newX, newY := p.X + dx, p.Y + dy

	if cell, prs := cmap[Pos{newX, newY}];
	prs && (cell.Surf.Flags & SF_WALKABLE) != 0 {
		p.X = newX
		p.Y = newY
	}
}
